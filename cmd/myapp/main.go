package main

import (
	"fmt"
	"os"
	"strconv"
	"unicode"
	"unicode/utf8"
)

func main() {
	if len(os.Args) != 2 {
		fmt.Println("usage: ./isChar `char`")
		return
	}

	if utf8.RuneCountInString(os.Args[1]) != 1 {
		fmt.Printf("arg[1]=`%s` is not single char\n", os.Args[1])
		return
	}

	if !unicode.IsDigit([]rune(os.Args[1])[0]) {
		fmt.Println("not digit")
		return
	}

	dig, err := strconv.Atoi(os.Args[1])
	if err != nil {
		fmt.Printf("error: %v", err)
		return
	}

	fmt.Println(dig)
}
